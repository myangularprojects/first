import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public subject:any ="angular is  a type script framework "

  public user:any={
    name:"dharani",
    email:"dharandharani830@gmail.com",
    country:"India"
  }

  public userList:any=[
    {
    name:"dharani",
    email:"dharandharani830@gmail.com",
    country:"India"
    },
    {
    name:"dharanidharan",
    email:"dharandharani830@gmail.com",
    country:"India"
    },
    {
      name:"R.dharani",
      email:"dharandharani830@gmail.com",
      country:"India"
    }
  ]
}
