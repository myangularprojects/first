import { Component } from '@angular/core';
import { ColDef } from 'ag-grid-community';

@Component({
  selector: 'app-dataservice',
  templateUrl: './dataservice.component.html',
  styleUrls: ['./dataservice.component.css']
})
export class DataserviceComponent 
{
  public columnDefs: ColDef[] = [
    { field: 'name'},
    { field: 'type'}
    
  ];

  public defaultColDef: ColDef = {
    sortable: true,
    filter: true,
  };
  
  public database:any=[
    {
      dataSourceId:100,
      dbType:"mysql",
      dbName:"db-one",
      dataService:[
        {
          id:100,
          dsName:"employee-info",
          createdBy:"admin",
          createdDate:"20-12-2023",
          columns:[
            {
              name:"dharani",
              type:"String"
            },
             {
              name:"ambani",
              type:"String"
            },
             {
              name:"bill gates",
              type:"String"
            },
             {
              name:"steve jobs",
              type:"String"
            }
          ]
        },
        {
          id:100,
          dsName:"employee-dept",
          createdBy:"admin",
          createdDate:"20-12-2023",
          columns:[
            {
              name:"paint",
              type:"String"
            },
             {
              name:"petrol",
              type:"String"
            },
             {
              name:"microsoft",
              type:"String"
            },
             {
              name:"apple",
              type:"String"
            }
          ]
        }
      ]
    },
    {
      dataSourceId:200,
      dbType:"mysql",
      dbName:"db-two",
      dataService:[
        {
          id:100,
          dsName:"student-info",
          createdBy:"admin",
          createdDate:"20-12-2023",
          columns:[
            {
              name:"dharanidharan",
              type:"String"
            },
             {
              name:"ajit agarkar",
              type:"String"
            },
             {
              name:"mary comb",
              type:"String"
            },
             {
              name:"steve hawk",
              type:"String"
            }
          ]
        },
        {
          id:100,
          dsName:"student-institute",
          createdBy:"admin",
          createdDate:"20-12-2023",
          columns:[
            {
              name:"jspiders",
              type:"String"
            },
             {
              name:"cricket center",
              type:"String"
            },
             {
              name:"kick boxing center",
              type:"String"
            },
             {
              name:"science school",
              type:"String"
            }
          ]
        }
      ]
    },
    {
      dataSourceId:300,
      dbType:"oraclesql",
      dbName:"db-1",
      dataService:[
        {
          id:200,
          dsName:"employee-info",
          createdBy:"admin",
          createdDate:"20-12-2023",
          columns:[
            {
              name:"vignesh",
              type:"String"
            },
             {
              name:"singh",
              type:"String"
            },
             {
              name:"boopathi",
              type:"String"
            },
             {
              name:"bijili",
              type:"String"
            }
          ]
        },
        {
          id:200,
          dsName:"employee-dept",
          createdBy:"admin",
          createdDate:"20-12-2023",
          columns:[
            {
              name:"powerTrain",
              type:"String"
            },
             {
              name:"pawn shop",
              type:"String"
            }, {
              name:"driving class",
              type:"String"
            },
             {
              name:"crackers store",
              type:"String"
            }
          ]
        }
      ]
    },
    {
      dataSourceId:400,
      dbType:"oraclesql",
      dbName:"db-2",
      dataService:[
        {
          id:200,
          dsName:"student-info",
          createdBy:"admin",
          createdDate:"20-12-2023",
          columns:[
            {
              name:"karthi",
              type:"String"
            }, {
              name:"sivanaandi",
              type:"String"
            },
             {
              name:"moorthy",
              type:"String"
            },
             {
              name:"mukundhan",
              type:"String"
            }
          ]
        },
        {
          id:200,
          dsName:"student-institute",
          createdBy:"admin",
          createdDate:"20-12-2023",
          columns:[
            {
              name:"selvam clg",
              type:"String"
            },
             {
              name:"psg clg",
              type:"String"
            },
             {
              name:"sns clg",
              type:"String"
            },
             {
              name:"paavai clg",
              type:"String"
            }
          ]
        }
      ]
    }
  ];
  public filteredDataType:any=[];
  public filteredDataBase:any=[];
  public filteredTableData:any=[];
  public filteredColumnData:any=[];
  public dataSource:any;
  public array:any;
  ngOnInit():void{
  let data=this.database.map((each:any)=>{
    return each['dbType'];
  })
   let set=new Set(data);
   this.filteredDataType=[...set];
  }; 
  
  public getDataBase(event:any){
   this.filteredDataBase= this.database.filter((each:any)=>{
    return each['dbType']===event.target.value;
    });
  }
  public getTableData(event:any){
    this.filteredTableData= this.filteredDataBase.filter((each:any)=>{
     return each['dbName']===event.target.value;
     });
    for (const fd of this.filteredTableData) {
      this.dataSource= fd.dataService
    }
    }
  public getColumnData(event:any){
      for (let ftd of this.filteredDataBase) {
       for (let fd of ftd.dataService) {
        if(fd.dsName===event.target.value){
        this.array=fd.columns;
      }
      }
    }
  
      }
}
