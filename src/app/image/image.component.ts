import { Component } from '@angular/core';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent {
public title:any="Indian Flag";
public imageUrl:any="https://i.pinimg.com/originals/61/17/ee/6117ee89fbe7f40796fc7f17dfcf6475.jpg";

public updateImage(){
  this.imageUrl="https://th.bing.com/th?id=OSK.bbf94b1a10da3a02fe7300ced0da65af&w=148&h=148&c=7&o=6&pid=SANGAM";
}
}
