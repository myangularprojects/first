import { Component } from '@angular/core';
import { ColDef } from 'ag-grid-community';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent {
  public columnDefs: ColDef[] = [
    {field:"productId"},
    {field:"productName"},
    {field:"productPrice"},
    {field:"productBrand"},
    {field:"productQuantity"}
  ];

  public defaultColDef: ColDef = {
    sortable: true,
    filter: true,
  };
 

 public productList:any=[
  {
    "productId"      :101,
    "productName"    :"note 7",
    "productPrice"   :10000,
    "productBrand"   :"redmi",
    "productQuantity":7,

  },
  {
    "productId"      :102,
    "productName"    :"note 8",
    "productPrice"   :13000,
    "productBrand"   :"redmi",
    "productQuantity":8,
  },
  {
    "productId"      :103,
    "productName"    :"note 10",
    "productPrice"   :15000,
    "productBrand"   :"redmi",
    "productQuantity":10,
  }
 ];
 
 
public reduceQuantity(productid:any){
//     for (let index = 0; index < this.productList.length; index++) {
//       if(this.productList[index].productId===productid)
//       this.productList[index].productQuantity+=-1;
//     }
this.productList.map((each:any)=>{
        if(each['productId']===productid){
        if(each['productQuantity']>0)
         each['productQuantity']--;
         else alert("quantity is not enough");}
         return each;

});
    
}
public filteredProductList:any=[];

constructor(){
  this.filteredProductList=this.productList;
}


public getProductsByBrand(event:any){
  this.filteredProductList=this.productList.filter((each:any)=>
  {
  return each['productName']===event.target.value;
  });

}
}
